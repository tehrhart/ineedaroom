import datetime
import os
import platform

import keyring
import pytest

from ineedaroom.ineedaroom import (
    BASE_URL,
    CredentialManager,
    EURECOMSession,
    InvalidLocationHeader,
    InvalidResponseStatusCode,
    RoomBooking,
    RoomBookingError,
    extract_duration,
    format_date,
    get_date_range_from_input,
    get_user_choice,
    main,
    parse_args,
)


@pytest.fixture
def mock_get_date_range_from_input(monkeypatch):
    monkeypatch.setattr(
        'ineedaroom.ineedaroom.get_date_range_from_input',
        lambda input: (datetime.datetime(2023, 10, 1, 10, 0), datetime.datetime(2023, 10, 1, 11, 0)),
    )


@pytest.fixture
def mock_get_user_choice(monkeypatch):
    monkeypatch.setattr('ineedaroom.ineedaroom.get_user_choice', lambda rooms, available_rooms: 1)


@pytest.fixture
def mock_get_user_invalid_choice(monkeypatch):
    monkeypatch.setattr('ineedaroom.ineedaroom.get_user_choice', lambda rooms, available_rooms: None)


@pytest.fixture
def mock_format_date(monkeypatch):
    monkeypatch.setattr('ineedaroom.ineedaroom.format_date', lambda date: "Sunday October 01, 2023 at 10:00 AM")


@pytest.fixture
def mock_get_calendar_data(monkeypatch):
    monkeypatch.setattr('ineedaroom.ineedaroom.EURECOMSession.get_calendar_data', lambda self, start_date, resource: [])


@pytest.fixture
def mock_get_meeting_rooms(monkeypatch):
    monkeypatch.setattr(
        'ineedaroom.ineedaroom.EURECOMSession.get_meeting_rooms',
        lambda self: {1: "Meeting Room 1", 2: "Meeting Room 2"},
    )


@pytest.fixture
def mock_find_closest_available_time(monkeypatch):
    monkeypatch.setattr(
        'ineedaroom.ineedaroom.RoomBooking.find_closest_available_time',
        lambda rooms, start_datetime, end_datetime, calendar_data: (
            [1],
            datetime.datetime(2023, 10, 1, 10, 0),
        ),
    )


@pytest.fixture
def mock_find_closest_available_time_different_time(monkeypatch):
    monkeypatch.setattr(
        'ineedaroom.ineedaroom.RoomBooking.find_closest_available_time',
        lambda rooms, start_datetime, end_datetime, calendar_data: (
            [1],
            datetime.datetime(2023, 10, 1, 10, 30),
        ),
    )


@pytest.fixture
def mock_book_room(monkeypatch):
    monkeypatch.setattr(
        'ineedaroom.ineedaroom.EURECOMSession.book_room',
        lambda self, room_id, available_datetime, end_date, user_id, event_csrf_token: 1234,
    )


@pytest.fixture
def mock_book_room_failure_already_booked(monkeypatch):
    def book_room(self, room_id, available_datetime, end_date, user_id, event_csrf_token):
        raise InvalidResponseStatusCode()

    monkeypatch.setattr('ineedaroom.ineedaroom.EURECOMSession.book_room', book_room)


@pytest.fixture
def mock_book_room_failure_invalid_location_header(monkeypatch):
    def book_room(self, room_id, available_datetime, end_date, user_id, event_csrf_token):
        raise InvalidLocationHeader()

    monkeypatch.setattr('ineedaroom.ineedaroom.EURECOMSession.book_room', book_room)


@pytest.fixture
def mock_book_room_failure_unexpected(monkeypatch):
    def book_room(self, room_id, available_datetime, end_date, user_id, event_csrf_token):
        raise RoomBookingError()

    monkeypatch.setattr('ineedaroom.ineedaroom.EURECOMSession.book_room', book_room)


@pytest.fixture
def mock_load_credentials(monkeypatch):
    monkeypatch.setattr('ineedaroom.ineedaroom.CredentialManager.load_credentials', lambda self: None)


@pytest.fixture
def mock_delete_credentials(monkeypatch):
    monkeypatch.setattr('ineedaroom.ineedaroom.CredentialManager.delete_credentials', lambda self: None)


@pytest.fixture
def mock_load_invalid_credentials(monkeypatch):
    def load_credentials(self):
        raise ValueError("Invalid credentials")

    monkeypatch.setattr('ineedaroom.ineedaroom.CredentialManager.load_credentials', load_credentials)


@pytest.fixture
def mock_initialize_credentials(monkeypatch):
    monkeypatch.setattr('ineedaroom.ineedaroom.CredentialManager.initialize_credentials', lambda self: None)


@pytest.fixture
def mock_find_closest_available_time_no_rooms(monkeypatch):
    monkeypatch.setattr(
        'ineedaroom.ineedaroom.RoomBooking.find_closest_available_time',
        lambda rooms, start_datetime, end_datetime, calendar_data: ([], None),
    )


@pytest.fixture
def mock_login(monkeypatch):
    monkeypatch.setattr('ineedaroom.ineedaroom.EURECOMSession.login', lambda self, username, password: None)


@pytest.fixture
def mock_get_user_id_and_token(monkeypatch):
    monkeypatch.setattr('ineedaroom.ineedaroom.EURECOMSession.get_user_id_and_token', lambda self: (1, "test_token"))


def test_login_csrf_token_not_found(requests_mock):
    session = EURECOMSession()
    requests_mock.get(f"{BASE_URL}/sso/login", text="<html></html>")
    with pytest.raises(ValueError, match="Login CSRF token not found"):
        session.login("username", "password")


def test_save_credentials(monkeypatch):
    def mock_set_password(service_name, username_key, username_value):
        assert service_name == "ineedaroom"
        assert username_key in ["username", "test_user"]
        if username_key == "username":
            assert username_value == "test_user"
        elif username_key == "test_user":
            assert username_value == "test_pass"

    monkeypatch.setattr(keyring, "set_password", mock_set_password)

    cred_manager = CredentialManager()
    cred_manager.save_credentials("test_user", "test_pass")


def test_load_credentials(monkeypatch):
    def mock_get_password(service_name, username_key):
        assert service_name == "ineedaroom"
        if username_key == "username":
            return "test_user"
        elif username_key == "test_user":
            return "test_pass"
        return None

    monkeypatch.setattr(keyring, "get_password", mock_get_password)

    cred_manager = CredentialManager()
    cred_manager.load_credentials()

    assert cred_manager.username == "test_user"
    assert cred_manager.password == "test_pass"


def test_load_missing_credentials(monkeypatch):
    def mock_get_password(service_name, username_key):
        return None

    monkeypatch.setattr(keyring, "get_password", mock_get_password)

    cred_manager = CredentialManager()

    with pytest.raises(ValueError, match="No credentials stored in keyring"):
        cred_manager.load_credentials()


def test_delete_credentials(monkeypatch):
    def mock_delete_password(service_name, username_key):
        assert service_name == "ineedaroom"
        assert username_key in ["username", "valid_user"]

    monkeypatch.setattr(keyring, "get_password", lambda service_name, username_key: "valid_user")
    monkeypatch.setattr(keyring, "delete_password", mock_delete_password)

    cred_manager = CredentialManager()
    cred_manager.delete_credentials()


def test_delete_credentials_no_credentials(monkeypatch):
    def mock_delete_password(service_name, username_key):
        assert service_name == "ineedaroom"
        assert username_key == "username"

    monkeypatch.setattr(keyring, "get_password", lambda service_name, username_key: None)
    monkeypatch.setattr(keyring, "delete_password", mock_delete_password)

    cred_manager = CredentialManager()
    cred_manager.delete_credentials()


def test_get_calendar_data(requests_mock):
    session = EURECOMSession()
    start_date = "2023-10-01"
    resource = "12147"
    calendar_data = [
        {
            "room_id": [12147],
            "start_date": "10/01/2023 08:00",
            "end_date": "10/01/2023 09:00",
        }
    ]
    requests_mock.get(
        f"{BASE_URL}/calendar/events.dhtmlx?resource={resource}&timeshift=-120&from={start_date}T00:00:00&to={start_date}T23:59:59",
        json=calendar_data,
    )
    result = session.get_calendar_data(start_date, resource)
    assert result == calendar_data


def test_get_meeting_rooms(requests_mock):
    session = EURECOMSession()
    rooms_page_html = '''
    <html>
        <ul class="roomAttendees" data-prototype="&lt;li&gt;&lt;select id=&quot;event_resourceAttendees___name___resource&quot;&gt;&lt;option value=&quot;12147&quot;&gt;Meeting Room 1&lt;/option&gt;&lt;option value=&quot;1234&quot;&gt;Staff room&lt;/option&gt;&lt;/select&gt;&lt;/li&gt;">
        </ul>
    </html>
    '''
    requests_mock.get(f"{BASE_URL}/meetings/new", text=rooms_page_html)
    rooms = session.get_meeting_rooms()
    assert rooms == {12147: "Meeting Room 1"}


def test_book_room(requests_mock):
    session = EURECOMSession()
    requests_mock.post(f"{BASE_URL}/meetings/new", status_code=302, headers={'Location': '/meetings/1234'})
    result = session.book_room(
        12147, datetime.datetime.now(), datetime.datetime.now() + datetime.timedelta(hours=1), 1, "test_token"
    )
    assert result == 1234


def test_book_room_failure(requests_mock):
    session = EURECOMSession()
    requests_mock.post(f"{BASE_URL}/meetings/new", status_code=400)
    with pytest.raises(InvalidResponseStatusCode):
        session.book_room(
            12147, datetime.datetime.now(), datetime.datetime.now() + datetime.timedelta(hours=1), 1, "test_token"
        )

    requests_mock.post(f"{BASE_URL}/meetings/new", status_code=302, headers={'Location': 'wrong_location'})
    with pytest.raises(InvalidLocationHeader):
        session.book_room(
            12147, datetime.datetime.now(), datetime.datetime.now() + datetime.timedelta(hours=1), 1, "test_token"
        )


def test_is_room_available_past_start_datetime():
    past_datetime = datetime.datetime.now() - datetime.timedelta(days=1)
    future_datetime = datetime.datetime.now() + datetime.timedelta(days=1)
    result = RoomBooking.is_room_available(12147, past_datetime, future_datetime, [])
    assert not result


def test_get_user_id_and_token(requests_mock):
    session = EURECOMSession()
    booking_page_html = '''
    <html>
        <input type="hidden" name="event[_token]" value="test_token">
        <select name="event[peopleAttendees][0][person]">
            <option value="1" selected="selected">User</option>
        </select>
    </html>
    '''
    requests_mock.get(f"{BASE_URL}/meetings/new", text=booking_page_html)
    user_id, event_csrf_token = session.get_user_id_and_token()
    assert user_id == 1
    assert event_csrf_token == "test_token"


def test_login_failure_due_to_incorrect_credentials(requests_mock):
    session = EURECOMSession()
    requests_mock.get(f"{BASE_URL}/sso/login", text='<input type="hidden" name="_csrf_token" value="test_token">')
    requests_mock.post(f"{BASE_URL}/sso/login", text="", cookies={})
    with pytest.raises(ValueError, match="Login failed. Please check your credentials."):
        session.login("username", "password")


def test_initialize_credentials_with_invalid_input(monkeypatch, tmp_path):
    manager = CredentialManager()

    inputs = iter(["invalid_user", "invalid_pass", "valid_user", "valid_pass"])
    monkeypatch.setattr('builtins.input', lambda _: next(inputs))
    monkeypatch.setattr('getpass.getpass', lambda prompt: next(inputs))

    monkeypatch.setattr(
        EURECOMSession,
        "login",
        lambda self, username, password: (
            None
            if (username == "valid_user" and password == "valid_pass")
            else (_ for _ in ()).throw(ValueError("Invalid credentials"))
        ),
    )

    manager.initialize_credentials()

    assert manager.username == "valid_user"
    assert manager.password == "valid_pass"


def test_find_closest_available_time_no_rooms_available():
    rooms = {12147: "Meeting Room 1", 12148: "Meeting Room 2"}
    start_datetime = datetime.datetime.now() + datetime.timedelta(days=1)
    end_datetime = start_datetime + datetime.timedelta(hours=1)
    calendar_data = [
        {
            "room_id": [room_id],
            "start_date": (start_datetime - datetime.timedelta(minutes=30)).strftime("%m/%d/%Y %H:%M"),
            "end_date": (start_datetime + datetime.timedelta(hours=23, minutes=59)).strftime("%m/%d/%Y %H:%M"),
        }
        for room_id in rooms
    ]
    available_rooms, available_datetime = RoomBooking.find_closest_available_time(
        rooms, start_datetime, end_datetime, calendar_data
    )
    assert not available_rooms
    assert available_datetime is None


def test_find_closest_available_time_next_timeslot():
    rooms = {12147: "Meeting Room 1", 12148: "Meeting Room 2"}
    start_datetime = datetime.datetime.now() + datetime.timedelta(days=1)
    end_datetime = start_datetime + datetime.timedelta(hours=1)
    calendar_data = [
        {
            "room_id": [room_id],
            "start_date": (start_datetime - datetime.timedelta(minutes=30)).strftime("%m/%d/%Y %H:%M"),
            "end_date": (start_datetime + datetime.timedelta(minutes=30)).strftime("%m/%d/%Y %H:%M"),
        }
        for room_id in rooms
    ]
    available_rooms, available_datetime = RoomBooking.find_closest_available_time(
        rooms, start_datetime, end_datetime, calendar_data
    )
    assert available_rooms == list(rooms.keys())
    assert available_datetime == start_datetime + datetime.timedelta(minutes=30)


def test_get_user_choice(monkeypatch):
    monkeypatch.setattr('inquirer.prompt', lambda questions: {"choice": 1})
    rooms = {1: "Room 1", 2: "Room 2", 3: "Room 3"}
    available_rooms = [1, 3]
    selected_room = get_user_choice(rooms, available_rooms)
    assert selected_room == 1


def test_get_user_choice_canceled(monkeypatch):
    monkeypatch.setattr('inquirer.prompt', lambda questions: None)
    rooms = {1: "Room 1", 2: "Room 2", 3: "Room 3"}
    available_rooms = [1, 3]
    selected_room = get_user_choice(rooms, available_rooms)
    assert selected_room is None


def test_get_user_choice_unavailable(monkeypatch, capfd):
    def fake_prompt(questions):
        fake_prompt.call_count += 1
        return {"choice": 2} if fake_prompt.call_count == 1 else {"choice": 3}

    fake_prompt.call_count = 0
    monkeypatch.setattr('inquirer.prompt', fake_prompt)

    rooms = {1: "Room 1", 2: "Room 2", 3: "Room 3"}
    available_rooms = [1, 3]
    selected_room = get_user_choice(rooms, available_rooms)

    assert "This room is not available. Please choose another room." in capfd.readouterr().out
    assert selected_room == 3


def test_get_date_range_from_input():
    test_cases = [
        (
            "10:00",
            datetime.datetime.now().replace(hour=10, minute=0, second=0, microsecond=0),
            datetime.datetime.now().replace(hour=11, minute=0, second=0, microsecond=0),
        ),
        (
            "tomorrow at 2pm",
            datetime.datetime.now().replace(hour=14, minute=0, second=0, microsecond=0) + datetime.timedelta(days=1),
            datetime.datetime.now().replace(hour=15, minute=0, second=0, microsecond=0) + datetime.timedelta(days=1),
        ),
        (
            "today at 3pm or tomorrow",
            datetime.datetime.now().replace(hour=15, minute=0, second=0, microsecond=0),
            datetime.datetime.now().replace(hour=16, minute=0, second=0, microsecond=0),
        ),
        (
            "tomorrow at 9am for 2 hours",
            datetime.datetime.now().replace(hour=9, minute=0, second=0, microsecond=0) + datetime.timedelta(days=1),
            datetime.datetime.now().replace(hour=11, minute=0, second=0, microsecond=0) + datetime.timedelta(days=1),
        ),
    ]

    for input_str, expected_start, expected_end in test_cases:
        start_datetime, end_datetime = get_date_range_from_input(input_str)
        assert start_datetime == expected_start
        assert end_datetime == expected_end


def test_get_date_range_from_input_with_invalid_input():
    start_datetime, end_datetime = get_date_range_from_input("invalid")
    assert start_datetime.replace(second=0, microsecond=0) == (
        datetime.datetime.now() + datetime.timedelta(minutes=1)
    ).replace(second=0, microsecond=0)
    assert end_datetime.replace(second=0, microsecond=0) == (
        datetime.datetime.now() + datetime.timedelta(hours=1, minutes=1)
    ).replace(second=0, microsecond=0)


def test_format_date():
    date = datetime.datetime(2023, 10, 1, 10, 0)
    formatted_date = format_date(date)
    assert formatted_date == "Sunday October 01, 2023"


@pytest.mark.parametrize(
    "text, expected_duration, expected_rest",
    [
        # Valid cases
        ("1h30min", 90, ""),  # 1 hour 30 minutes
        ("1h30", 90, ""),  # 1 hour 30 minutes (no "min")
        ("1h", 60, ""),  # 1 hour
        ("30min", 30, ""),  # 30 minutes
        ("30m", 30, ""),  # 30 minutes (alternate format)
        # No duration
        ("No duration here", None, "No duration here"),
        # Edge cases
        ("0h15m", 15, ""),  # Zero hours
        ("0m", 0, ""),  # Exactly 0 minutes
        ("", None, ""),  # Empty string
    ],
)
def test_extract_duration(text, expected_duration, expected_rest):
    duration, rest = extract_duration(text)
    assert duration == expected_duration
    assert rest == expected_rest


@pytest.fixture
def mock_argv(monkeypatch):
    monkeypatch.setattr('sys.argv', ["ineedaroom.py", "10:00"])


@pytest.mark.usefixtures(
    "mock_argv",
    "mock_get_date_range_from_input",
    "mock_get_user_choice",
    "mock_format_date",
    "mock_get_calendar_data",
    "mock_get_meeting_rooms",
    "mock_find_closest_available_time",
    "mock_book_room",
    "mock_load_credentials",
    "mock_initialize_credentials",
    "mock_login",
    "mock_get_user_id_and_token",
)
def test_main(monkeypatch):
    main()

    # Test that the pause command is called on Windows
    def mock_os_system(cmd):
        return mock_os_system.call_args.append(cmd)

    monkeypatch.setattr(platform, "system", lambda: "Windows")
    mock_os_system.call_args = []
    monkeypatch.setattr(os, "system", mock_os_system)
    main()
    assert mock_os_system.call_args == ["pause"]


@pytest.mark.usefixtures(
    "mock_argv",
    "mock_get_date_range_from_input",
    "mock_get_user_choice",
    "mock_format_date",
    "mock_get_calendar_data",
    "mock_get_meeting_rooms",
    "mock_find_closest_available_time_different_time",
    "mock_book_room",
    "mock_load_credentials",
    "mock_initialize_credentials",
    "mock_login",
    "mock_get_user_id_and_token",
)
def test_main_different_time():
    main()


@pytest.mark.usefixtures(
    "mock_argv",
    "mock_get_date_range_from_input",
    "mock_get_user_choice",
    "mock_format_date",
    "mock_get_calendar_data",
    "mock_get_meeting_rooms",
    "mock_find_closest_available_time_no_rooms",
    "mock_load_credentials",
    "mock_initialize_credentials",
    "mock_login",
    "mock_get_user_id_and_token",
)
def test_main_no_rooms_available(capfd):
    with pytest.raises(SystemExit):
        main()
    assert "No rooms available this day. Exiting." in capfd.readouterr().out


@pytest.mark.usefixtures(
    "mock_argv",
    "mock_get_date_range_from_input",
    "mock_get_user_choice",
    "mock_format_date",
    "mock_get_calendar_data",
    "mock_get_meeting_rooms",
    "mock_find_closest_available_time",
    "mock_book_room",
    "mock_load_invalid_credentials",
    "mock_initialize_credentials",
    "mock_login",
    "mock_get_user_id_and_token",
)
def test_main_no_credentials():
    main()


@pytest.mark.usefixtures(
    "mock_argv",
    "mock_get_date_range_from_input",
    "mock_get_user_invalid_choice",
    "mock_format_date",
    "mock_get_calendar_data",
    "mock_get_meeting_rooms",
    "mock_find_closest_available_time",
    "mock_book_room",
    "mock_load_invalid_credentials",
    "mock_initialize_credentials",
    "mock_login",
    "mock_get_user_id_and_token",
)
def test_main_no_room_selected(capfd):
    with pytest.raises(SystemExit):
        main()
    assert "No room selected. Exiting." in capfd.readouterr().out


@pytest.mark.usefixtures(
    "mock_argv",
    "mock_get_date_range_from_input",
    "mock_get_user_choice",
    "mock_format_date",
    "mock_get_calendar_data",
    "mock_get_meeting_rooms",
    "mock_find_closest_available_time",
    "mock_book_room_failure_already_booked",
    "mock_load_credentials",
    "mock_initialize_credentials",
    "mock_login",
    "mock_get_user_id_and_token",
)
def test_main_room_already_booked(capfd):
    main()
    assert "Error: Room booking failed. The room may already be booked." in capfd.readouterr().out


@pytest.mark.usefixtures(
    "mock_argv",
    "mock_get_date_range_from_input",
    "mock_get_user_choice",
    "mock_format_date",
    "mock_get_calendar_data",
    "mock_get_meeting_rooms",
    "mock_find_closest_available_time",
    "mock_book_room_failure_invalid_location_header",
    "mock_load_credentials",
    "mock_initialize_credentials",
    "mock_login",
    "mock_get_user_id_and_token",
)
def test_main_room_already_booked_invalid_location_header(capfd):
    main()
    assert "Error: Unable to book the room due to a missing or invalid Location header." in capfd.readouterr().out


@pytest.mark.usefixtures(
    "mock_argv",
    "mock_get_date_range_from_input",
    "mock_get_user_choice",
    "mock_format_date",
    "mock_get_calendar_data",
    "mock_get_meeting_rooms",
    "mock_find_closest_available_time",
    "mock_book_room_failure_unexpected",
    "mock_load_credentials",
    "mock_initialize_credentials",
    "mock_login",
    "mock_get_user_id_and_token",
)
def test_main_room_booking_failure_unexpected(capfd):
    main()
    assert "Error: An unexpected booking error occurred. Details:" in capfd.readouterr().out


def test_parse_args():
    args = parse_args(["10:00"])
    assert args.user_input == ["10:00"]
    assert not args.logout

    args = parse_args(["--logout"])
    assert args.user_input == []
    assert args.logout


@pytest.mark.usefixtures("mock_delete_credentials")
def test_main_logout(monkeypatch, capfd):
    monkeypatch.setattr('sys.argv', ['ineedaroom.py', '--logout'])
    with pytest.raises(SystemExit):
        main()
        assert "Removing login credentials." in capfd.readouterr().out
