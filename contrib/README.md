`tutorial.gif` was generated with [`vhs`](https://github.com/charmbracelet/vhs) using the following command:

```bash
vhs tutorial.tape -o tutorial.gif
```
