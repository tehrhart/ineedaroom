#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import datetime
import getpass
import html
import importlib.metadata
import os
import platform
import re
import sys
from typing import Any, Dict, List, Optional, Tuple

import inquirer
import keyring
import requests
from bs4 import BeautifulSoup
from timefhuman import timefhuman

# Base URL for the EURECOM events website
BASE_URL = "https://events.eurecom.fr"

# Regular expression to find hours and minutes variations
DURATION_PATTERN = re.compile(
    r'\b(\d+)\s*h(?:ours?)?\s*(\d+)?\s*m?(?:in(?:utes?)?)?\b|\b(\d+)\s*m(?:in(?:utes?)?)?\b', re.IGNORECASE
)


def format_date(date: datetime) -> str:
    return date.strftime("%A %B %d, %Y")


class RoomBookingError(Exception):
    """Base class for other room booking exceptions"""

    pass


class InvalidResponseStatusCode(RoomBookingError):
    """Raised when the response status code is not as expected"""

    pass


class InvalidLocationHeader(RoomBookingError):
    """Raised when the Location header in the response is missing or not as expected"""

    pass


class CredentialManager:
    def __init__(self):
        self.service_name = "ineedaroom"
        self.username = None
        self.password = None

    def save_credentials(self, username: str, password: str) -> None:
        """Save credentials to the system's keyring"""
        keyring.set_password(self.service_name, "username", username)
        keyring.set_password(self.service_name, username, password)

    def load_credentials(self) -> None:
        """Load credentials from the system's keyring"""
        try:
            self.username = keyring.get_password(self.service_name, "username")
            self.password = keyring.get_password(self.service_name, self.username)
            if self.username is None or self.password is None:
                raise ValueError("No credentials stored in keyring")
        except Exception as e:
            raise ValueError(f"Error loading credentials: {e}") from None

    def delete_credentials(self) -> None:
        """Delete credentials from the system's keyring"""
        username = keyring.get_password(self.service_name, "username")
        if username:
            keyring.delete_password(self.service_name, username)
            keyring.delete_password(self.service_name, "username")

    def initialize_credentials(self) -> Tuple[str, str]:
        print("Welcome to I Need A Room, a tool to book meeting rooms at EURECOM.")
        print("Since this is your first time, we need to set up your credentials.")
        print(f"Please enter your EURECOM credentials. They are used for signing into {BASE_URL}.")
        print("Your credentials will be stored securely on your computer only.")

        session_manager = EURECOMSession()
        while True:
            username = input("Username: ")
            password = getpass.getpass(prompt="Password: ")
            print("Checking credentials...")
            try:
                session_manager.login(username, password)
                break
            except ValueError as e:
                print(f"Error: {e}")
                continue

        self.username = username
        self.password = password
        self.save_credentials(username, password)


class EURECOMSession:
    def __init__(self):
        self.session = requests.Session()

    def get_csrf_token(self, html_content: str, input_name: str) -> Optional[str]:
        soup = BeautifulSoup(html_content, "html.parser")
        csrf_token_input = soup.find("input", {"name": input_name})
        return csrf_token_input["value"] if csrf_token_input else None

    def login(self, username: str, password: str) -> None:
        self.session = requests.Session()
        login_page = self.session.get(f"{BASE_URL}/sso/login")
        login_csrf_token = self.get_csrf_token(login_page.text, "_csrf_token")
        if not login_csrf_token:
            raise ValueError("Login CSRF token not found")

        login_data = {
            "username": username,
            "password": password,
            "_csrf_token": login_csrf_token,
        }
        self.session.post(f"{BASE_URL}/sso/login", data=login_data)
        if not self.session.cookies.get("X-Jwt"):
            raise ValueError("Login failed. Please check your credentials.")

    def get_calendar_data(self, start_date: str, resource: str) -> List[Dict[str, Any]]:
        calendar_url = f"{BASE_URL}/calendar/events.dhtmlx?resource={resource}&timeshift=-120&from={start_date}T00:00:00&to={start_date}T23:59:59"
        calendar_page = self.session.get(calendar_url)
        return calendar_page.json()

    def get_meeting_rooms(self) -> Dict[int, str]:
        rooms = {}
        rooms_page = self.session.get(f"{BASE_URL}/meetings/new")
        soup = BeautifulSoup(rooms_page.text, "html.parser")
        # Extract the data-prototype attribute
        data_prototype = soup.find('ul', class_='roomAttendees')['data-prototype']
        # Decode the HTML entities in the data-prototype attribute
        decoded_prototype = html.unescape(data_prototype)
        # Parse the decoded HTML with BeautifulSoup
        prototype_soup = BeautifulSoup(decoded_prototype, 'html.parser')
        # Extract the room select element
        room_select = prototype_soup.find("select", {"id": "event_resourceAttendees___name___resource"})
        for option in room_select.find_all("option"):
            # We don't want all the other rooms
            if not option.text.lower().startswith("meeting room "):
                continue
            room_id = int(option["value"])
            room_name = option.text
            rooms[room_id] = room_name
        return rooms

    def book_room(
        self,
        room_id: int,
        available_datetime: datetime.datetime,
        end_date: datetime.datetime,
        user_id: int,
        event_csrf_token: str,
    ) -> int:
        book_data = {
            "event[labelFr]": "Meeting",
            "event[beginDate][date]": available_datetime.strftime("%Y-%m-%d"),
            "event[beginDate][time]": available_datetime.strftime("%H:%M"),
            "event[endDate][date]": end_date.strftime("%Y-%m-%d"),
            "event[endDate][time]": end_date.strftime("%H:%M"),
            "event[resourceAttendees][1][resource]": room_id,
            "event[peopleAttendees][0][person]": user_id,
            "event[_token]": event_csrf_token,
        }
        response = self.session.post(f"{BASE_URL}/meetings/new", data=book_data, allow_redirects=False)
        if response.status_code != 302:
            raise InvalidResponseStatusCode("Failed to book the room: Invalid response status code.")
        if "Location" not in response.headers or "/meetings/" not in response.headers["Location"]:
            raise InvalidLocationHeader("Failed to book the room: Missing or invalid Location header.")
        return int(response.headers["Location"].split("/")[-1])

    def get_user_id_and_token(self) -> Tuple[int, str]:
        booking_page = self.session.get(f"{BASE_URL}/meetings/new").text
        event_csrf_token = self.get_csrf_token(booking_page, "event[_token]")
        soup = BeautifulSoup(booking_page, "html.parser")
        user_id_select = soup.find("select", {"name": "event[peopleAttendees][0][person]"})
        user_id = int(user_id_select.find("option", {"selected": "selected"})["value"])
        return user_id, event_csrf_token


class RoomBooking:
    @staticmethod
    def is_room_available(
        room_id: int,
        start_datetime: datetime.datetime,
        end_datetime: datetime.datetime,
        calendar_data: List[Dict[str, Any]],
    ) -> bool:
        if start_datetime < datetime.datetime.now():
            return False
        for event in calendar_data:
            event_start = datetime.datetime.strptime(event["start_date"], "%m/%d/%Y %H:%M")
            event_end = datetime.datetime.strptime(event["end_date"], "%m/%d/%Y %H:%M")
            if room_id in event["room_id"] and start_datetime < event_end and end_datetime > event_start:
                return False
        return True

    @staticmethod
    def get_available_rooms(
        rooms: Dict[int, str],
        start_datetime: datetime.datetime,
        end_datetime: datetime.datetime,
        calendar_data: List[Dict[str, Any]],
    ) -> List[Tuple[int, str]]:
        available_rooms = []
        for room_id in rooms.keys():
            if RoomBooking.is_room_available(room_id, start_datetime, end_datetime, calendar_data):
                available_rooms.append(room_id)
        return available_rooms

    @staticmethod
    def find_closest_available_time(
        rooms: Dict[int, str],
        start_datetime: datetime.datetime,
        end_datetime: datetime.datetime,
        calendar_data: List[Dict[str, Any]],
    ) -> Tuple[List[Tuple[int, str]], datetime.datetime]:
        closest_available_time = None
        while True:
            available_rooms = RoomBooking.get_available_rooms(rooms, start_datetime, end_datetime, calendar_data)
            if available_rooms:
                return available_rooms, (closest_available_time if closest_available_time else start_datetime)
            else:
                start_datetime += datetime.timedelta(minutes=1)
                end_datetime += datetime.timedelta(minutes=1)
                closest_available_time = start_datetime
            # If no available time slots are found for the day, give up
            if start_datetime.hour == 23:
                return [], None


def extract_duration(text: str) -> Tuple[Optional[int], str]:
    # Search for the duration in the text
    match = DURATION_PATTERN.search(text)

    if not match:
        # No match found, return original text
        return None, text

    hours, minutes = 0, 0

    if match.group(1):
        hours = int(match.group(1))
        if match.group(2):
            minutes = int(match.group(2))
    elif match.group(3):
        minutes = int(match.group(3))

    # Convert the duration to minutes
    duration_in_minutes = hours * 60 + minutes

    # Remove the matched duration from the text
    rest = DURATION_PATTERN.sub('', text, count=1).strip()

    return duration_in_minutes, rest


def get_date_range_from_input(input: str) -> Tuple[datetime.datetime, datetime.datetime]:
    # Extract the duration and the rest of the text
    duration, rest = extract_duration(input)
    parsed_datetime = timefhuman(rest)
    # Default to right now + 1 minute
    start_datetime = datetime.datetime.now() + datetime.timedelta(minutes=1)
    if isinstance(parsed_datetime, list) and parsed_datetime and isinstance(parsed_datetime[0], datetime.datetime):
        start_datetime = parsed_datetime[0]
    elif isinstance(parsed_datetime, datetime.datetime):
        start_datetime = parsed_datetime
    # Default to 1 hour duration
    end_datetime = start_datetime + datetime.timedelta(minutes=duration if duration else 60)
    return start_datetime, end_datetime


def get_user_choice(rooms: Dict[int, str], available_rooms: List[int]) -> Optional[int]:
    rooms_choices = []
    for room_id, room_name in rooms.items():
        prefix = "\033[92m✅" if room_id in available_rooms else "\033[91m❌"
        text = f"{room_name} is available!" if room_id in available_rooms else f"{room_name} is taken."
        rooms_choices.append((f"{prefix} {text}\033[0m", room_id))

    questions = [
        inquirer.List(
            'choice',
            choices=rooms_choices,
            message="Select a room to book",
            carousel=True,
        )
    ]

    while True:
        selected_room_id = None
        answers = inquirer.prompt(questions)
        if not answers:
            break

        selected_room_id = answers['choice']
        if selected_room_id in available_rooms:
            break

        print("This room is not available. Please choose another room.")

    return selected_room_id


def parse_args(args: List[str]) -> argparse.Namespace:
    parser = argparse.ArgumentParser(description="Book a meeting room at EURECOM.")
    parser.add_argument(
        "-v", "--version", action="version", version=f"%(prog)s {importlib.metadata.version('ineedaroom')}"
    )
    parser.add_argument("--logout", action="store_true", help="Delete stored credentials and log out.")
    parser.add_argument("user_input", nargs="*", help="Date and time to book a room.")
    return parser.parse_args(args)


def main():
    args = parse_args(sys.argv[1:])

    # Initialize the credential manager
    credential_manager = CredentialManager()

    # Log out if the --logout flag is set
    if args.logout:
        print("Removing login credentials.")
        credential_manager.delete_credentials()
        sys.exit(0)

    # Load or initialize the credentials
    try:
        credential_manager.load_credentials()
    except ValueError:
        credential_manager.initialize_credentials()

    # Log in to the EURECOM website
    session_manager = EURECOMSession()
    session_manager.login(credential_manager.username, credential_manager.password)

    # Get the start date and time
    user_input = " ".join(os.sys.argv[1:])
    if not user_input:
        user_input = input("Enter a date/time (eg. 'now', '3pm', 'tomorrow at 10am'): ")
    start_datetime, end_datetime = get_date_range_from_input(user_input)
    start_date = start_datetime.strftime("%Y-%m-%d")
    print(
        f"Searching for a room for {format_date(start_datetime)} at {start_datetime.strftime('%H:%M')} - {end_datetime.strftime('%H:%M')}"
    )

    # Get the list of available rooms
    rooms = session_manager.get_meeting_rooms()

    # Get the calendar data for the day
    resource = "%2B".join(map(str, rooms.keys()))
    calendar_data = session_manager.get_calendar_data(start_date, resource)

    # Find the closest available time slot
    available_rooms, available_datetime = RoomBooking.find_closest_available_time(
        rooms, start_datetime, end_datetime, calendar_data
    )
    if not available_rooms or not available_datetime:
        print("\033[91mNo rooms available this day. Exiting.\033[0m")
        sys.exit(1)

    # If the closest available time slot is not the requested time, show the next available time slot
    if available_datetime != start_datetime:
        print("\033[91mNo rooms were available at the requested time 😔\033[0m")
        print()
        print(
            f"\033[1mNext available time slot: {format_date(available_datetime)} at {available_datetime.strftime('%H:%M')}\033[0m"
        )

    # Ask the user to select a room
    selected_room_id = get_user_choice(rooms, available_rooms)
    if selected_room_id is None:
        print("No room selected. Exiting.")
        sys.exit(1)

    # Book the room
    print(f"Booking: {rooms[selected_room_id]}...")
    user_id, event_csrf_token = session_manager.get_user_id_and_token()
    end_date = available_datetime + datetime.timedelta(hours=1)
    try:
        meeting_id = session_manager.book_room(
            selected_room_id, available_datetime, end_date, user_id, event_csrf_token
        )
        print(f"\033[92m✅ {rooms[selected_room_id]} has been booked!\033[0m")
        print(f"URL: {BASE_URL}/meetings/{meeting_id}")
    except InvalidResponseStatusCode:
        print(
            "\033[91mError: Room booking failed. The room may already be booked. Try running the program again.\033[0m"
        )
    except InvalidLocationHeader:
        print("\033[91mError: Unable to book the room due to a missing or invalid Location header.\033[0m")
    except RoomBookingError as e:
        print(f"\033[91mError: An unexpected booking error occurred. Details: {e}\033[0m")

    if platform.system() == "Windows":
        os.system("pause")


if __name__ == "__main__":
    main()
