<!-- markdownlint-configure-file {
  "MD013": {
    "code_blocks": false,
    "tables": false
  },
  "MD033": false,
  "MD041": false
} -->

<div align="center">

# I Need A Room

[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.eurecom.fr/tehrhart/ineedaroom/-/blob/main/LICENSE)
[![CI](https://gitlab.eurecom.fr/tehrhart/ineedaroom/badges/main/pipeline.svg)](https://gitlab.eurecom.fr/tehrhart/ineedaroom/-/pipelines)
[![Coverage](https://gitlab.eurecom.fr/tehrhart/ineedaroom/badges/main/coverage.svg)](https://gitlab.eurecom.fr/tehrhart/ineedaroom/-/pipelines)

I Need A Room (INAR) is a command line tool that helps you find and book a
meeting room at EURECOM.

[Getting started](#getting-started) •
[Installation](#installation)

</div>

## Getting started

![Tutorial](contrib/tutorial.gif)

```bash
ineedaroom 4pm

ineedaroom tomorrow at 9

ineedaroom friday 10am
```

## Installation

### Standalone executable

For Windows users, the easiest way to get started is by downloading the standalone executable. You can find the latest version on the [releases page](https://gitlab.eurecom.fr/tehrhart/ineedaroom/-/releases).

For other operating systems (macOS, Linux), you can install the tool using `pipx` or `pip`, as described below.

### Installation with `pipx`

For a more flexible installation, you can use [`pipx`](https://pipx.pypa.io/) to install `ineedaroom`.

```bash
pipx install ineedaroom --index-url https://gitlab.eurecom.fr/api/v4/projects/6364/packages/pypi/simple
```

Later on, you can update the tool to the latest version using the following command:

```bash
pipx upgrade ineedaroom
```

### Installation with `pip`

Alternatively, you can install `ineedaroom` using `pip`. This method is suitable if you're comfortable managing Python packages and have Python 3.9 or later installed:

```bash
pip install --user ineedaroom --index-url https://gitlab.eurecom.fr/api/v4/projects/6364/packages/pypi/simple
```

## Development

To install the development version of `ineedaroom`, clone the repository and
install the dependencies using [`poetry`](https://python-poetry.org/):

```bash
git clone https://gitlab.eurecom.fr/tehrhart/ineedaroom.git
cd ineedaroom
poetry install
poetry run pre-commit install
```

To run the program, use the following command:

```bash
poetry run ineedaroom
```

To run the tests, use the following command:

```bash
poetry run pytest --cov=ineedaroom
```

To package the program into a standalone executable, use the following command(s):

```bash
# Windows
poetry run cxfreeze bdist_msi

# macOS
poetry run cxfreeze bdist_dmg

# Linux
poetry run cxfreeze bdist_deb
```

### Releasing a new version

To release a new version of `ineedaroom`, follow these steps:

1. Update the project version number in `pyproject.tml`:

   ```toml
   [tool.poetry]
   version = "X.Y.Z"

   [tool.cxfreeze]
   metadata = {..., version = "X.Y.Z", ...}
   ```

1. Update the changelog in `CHANGELOG.md`:

   ```markdown
   ## [X.Y.Z] - YYYY-MM-DD

   ### Added

   - ...

   ### Changed

   - ...

   ### Fixed

   - ...
   ```

1. Commit the changes:

   ```bash
   git add pyproject.toml CHANGELOG.md
   git commit -m "chore: release X.Y.Z"
   git tag -a "X.Y.Z" -m "Release X.Y.Z"
   git push origin main --tags
   ```

1. The CI/CD pipeline will automatically create a new release on the
   [releases page](https://gitlab.eurecom.fr/tehrhart/ineedaroom/-/releases).

## License

This project is licensed under the terms of the MIT license. For more
information, see the [LICENSE](LICENSE) file.
