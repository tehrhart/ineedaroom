# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [1.1.0] - 2024-09-06

### Added

- `--logout` option to remove stored credentials.

### Changed

- Use `keyring` for credentials management.

## [1.0.0] - 2024-09-05

### Added

- Initial release of the project.
